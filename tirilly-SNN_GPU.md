# Implémentation de réseaux de neurones impulsionnels sur GPU

##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé
L'objectif de ce stage est de réaliser une implémentation sur GPU de modèles de réseaux de neurones impulsionnels. Dans un premier temps, une étude de faisabilité sur plusieurs frameworks candidats (Tensorflow, Cuda...) sera effectuée. La seconde partie du stage sera consacrée à l'implémentation effective de ces modèles selon la solution retenue.

### Mots-clés
Réseaux de neurones, neurones impulsionnels, graphical processing unit, GPU

### Encadrement

Équipe(s) : Fox

Encadrant(s) :
- Pierre Tirilly
- Pierre Falez

[Contacter les encadrants](mailto:pierre.tirilly@univ-lille.fr?subject=Stage%20de%20recherche%20%SNN%20sur%20GPU).

Localisation : IRCICA, Parc scientifique de la Haute-Borne, 59650 Villeneuve d'Ascq

##  Présentation détaillée

### Pré-requis
 - Formation en Informatique niveau Master 2 ou École d'ingénieur
 - Programmation : Python, C, C++
 - Bon niveau en rédaction technique
 - Des notions en programmation GPU ou en calcul numérique sont un plus
 - Des notions en apprentissage artificiel ou réseaux de neurones sont un plus

### Contexte
Les progrès récents en intelligence artificielle, très médiatisés, sont essentiellement dus au succès des réseaux de neurones artificiels, et plus particulièrement aux architectures profondes de réseaux de neurones (deep learning). Derrière ces succès applicatifs se cache une contrainte physique : la consommation énergétique de ces algorithmes. Les tailles de réseaux et les quantités de données croissant, simuler ces réseaux de neurones devient de plus en plus long et coûteux et nécessite des matériels adaptés, souvent des GPU (graphical processing units). Les réseaux de neurones impulsionnels (spiking neural networks, SNN) offrent une alternative à ce problème : il existe des composants ultra-basse consommation capable de les implémenter [Sourikopoulos et al., 2015]. Dans le cadre d'un projet pluridisciplinaire au sein de l'IRCICA, l'équipe Fox s'intéresse ainsi au développement de modèles de réseaux de neurones impulsionnels pour la vision par ordinateur [Falez et al., 2018a; Falez et al., 2018b]. Ce travail s'inscrit dans le cadre d'une thèse en collaboration avec l'équipe Émeraude et de l'axe de recherche Bioinspiré de l'IRCICA.

### Problématique
Malgré la possibilité à terme d'implémenter matériellement les SNN, le développement en amont de modèles spécifiques nécessite de passer par une étape de simulation coûteuse en temps de calcul. Les modèles implémentés s'exécutent actuellement sur CPU. Une parallélisation massive des simulations permettrait de réduire ces temps de simulation. L'objectif de ce stage est de produire une implémentation parallèle sur GPU des modèles existants. Pour cela, deux pistes peuvent être envisagées :
 - une implémentation à l'aide d'un framework générique de réseaux de neurones disposant déjà d'une implémentation GPU [Tensorflow; PyTorch; Theano]. Elle nécessite d'exprimer les modèles de SNN sous une forme compatible avec les interfaces du framework retenu ;
 - une implémentation directe sur GPU à l'aide des outils bas-niveau de programmation fournis par le fabricant [Cuda] ou d'outils intermédiaires [CudNN].

### Travail à effectuer
Le stage se déroulera en deux étapes :
1. Identification des outils candidats (frameworks de réseaux de neurones et bibliothèques de programmation GPU) et étude de la faisabilité d'une implémentation des SNN avec les outils considérés.
2. Implémentation effective des SNN selon la méthode retenue.

Chaque étape inclura la rédaction de la documentation technique appropriée.

### Bibliographie
[Sourikopoulos et al., 2017] I. Sourikopoulos, S. Hedayat, C. Loyez, F. Danneville, V. Hoel, E. Mercier, A. Cappy. A 4-fj/spike Artificial Neuron in 65 nm CMOS Technology. Frontiers in Neuroscience 11, 2017.

[Falez et al., 2018a] P. Falez, P. Tirilly, I.M. Bilasco, P. Devienne, P. Boulet. Mastering the Output Frequency of Spiking Neural Networks, International Joint Conference on Neural Networks, 2018.

[Falez et al., 2018a] P. Falez, P. Tirilly, I.M. Bilasco, P. Devienne, P. Boulet. STDP-based Feature Learning: How Far are we from Traditional Feature Learning Approaches?, preprint, 2018. Disponible sur demande.

[Cuda] https://developer.nvidia.com/cuda-zone

[CudNN] https://developer.nvidia.com/cudnn

[Tensorflow] https://www.tensorflow.org/

[Theano] http://deeplearning.net/software/theano/

[PyTorch] https://pytorch.org/