MOULINETTE := ../stages/moulinette/moulinette
PROMOTIONTITRE := ../stages/moulinette/promotion-titre

# Les .md dans l’ordre où ils ont été ajoutés
SRC := $(shell for i in *-*.md; do git log --pretty=format:"%ct $$i%n" "$$i" | tail -n1; done | sort | cut -d ' ' -f2-)

all: web web/index.mdwn $(patsubst %.md,web/%.pandocjson, $(SRC))

web:
	mkdir -p web

web/index.mdwn: $(SRC)
	$(MOULINETTE) $^ | pandoc -f json -M "dateModification:$$(date '+%e %B %Y')" -t markdown -s -o $@

web/%.pandocjson: %.md
	$(PROMOTIONTITRE) $< > $@

clean:
	rm -rf web

.PHONY: clean all
