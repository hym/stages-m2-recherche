#   Analyse de données de patients atteints de la maladie de Parkinson

##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé

La maladie de Parkinson représente plus de 150 000 cas en France et se place comme la 2ème cause de handicap moteur chez l'adulte. Elle se caractérise essentiellement par des tremblements, une certaine lenteur du mouvement, de la raideur musculaire et également une dégradation de la motricité fine. Le corps médical dispose de tests standards qui ne permettent pas de classifier isolément ces différents symptômes. 

A travers le projet [ParkEvolution](https://parkevolution.org/), nous étudions la motricité fine des patients en milieu écologique et de façon longitudinale à travers l'analyse de la position du curseur et des informations provenant de la souris d'ordinateur lors de l'utilisation de leur ordinateur personnel. Nous espérons que les résultats de ce projet permettront d'étudier l'évolution de la maladie des patients et l'efficacité des traitements.

### Mots-clés

IHM, Parkinson

### Encadrement

Équipe(s) : Loki

Encadrant(s) :

-   [Géry Casiez](http://cristal.univ-lille.fr/~casiez/)

[Contacter les encadrants](mailto:gery.casiez@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : Inria Lille – Nord Europe, bâtiment B


##  Présentation détaillée

### Pré-requis

Le candidat devra présenter des compétences en programmation et en analyse de données. Des aptitudes à la rédaction d'articles scientifiques seront appréciées.

### Contexte

L'analyse de la motricité fine de patients atteints de la maladie de Parkinson n'a encore jamais été réalisée dans un contexte écologique. Pour cela une première étape du projet a permis de collecter les données de patients atteints de la maladie et de personnes saines. L'étape suivante est d'analyser ces données. Le traitement de ces données nécessite des compétences dans les domaines de l'analyse de données, du clustering, de l'apprentissage et de l'IHM. Le but de ce projet sera alors à partir de ces données de comprendre et de classifier les désordres moteurs observés dans cette maladie.

### Problématique

L'analyse de données recueillies demande le développement de méthodes originales adaptées à ce contexte écologique.  

### Travail à effectuer

Le candidat sera impliqué dans l'analyse, l'organisation, la synthèse et la visualisation de l'ensemble des données recueillies auprès des patients inclus dans le protocole de recherche du projet ParkEvolution. 

### Bibliographie
