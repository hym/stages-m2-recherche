#   Indexation d’images pour la recherche par similarité en contexte mobile: une preuve de concept


##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé

La recherche d'images par le contenu (Content-Based Image Retrieval, CBIR) permet d'accéder aux images à partir de caractéristiques visuelles extraites de leurs pixels. Le stage proposé s’inscrit dans un contexte applicatif spécifique mobile (smartphone, tablette), qui considère des bases expertes (jeux d'images de *petites tailles* et relevant d'un *domaine métier donné*). Dans ce contexte, en se basant sur des travaux antérieurs (une thèse soutenue en novembre 2018), l'objectif de ce stage est de proposer une preuve de concept (POC) pour démontrer la faisabilité d'un système de recherche d'images.

### Mots-clés

Indexation, Recherche d’images par le contenu, Bases expertes

### Encadrement

Équipe : FOX

Encadrant(s) :

-   Patrick Perrois (Société Quadra Informatique, patrick.perrois@quadra-informatique.fr)
-   Hervé Vibert (Société Schneider Electric)
-   Jean Martinet (Université de Lille)

[Contacter les encadrants](mailto:patrick.perrois@quadra-informatique.fr?subject=Stage%20de%20recherche).

Localisation : Quadra Informatique, Synergie Park, 11 rue Louis Neel, 59260 Lezennes / Schneider Electric / IRCICA, Parc Scientifique de la Haute Borne, 50 Avenue Halley, 59658 Villeneuve d’Ascq


##  Présentation détaillée

### Pré-requis

Le candidat est un étudiant de Master 2 mention Informatique, idéalement IVI ou MOCAD, ou École d'ingénieur, et montre un grand intérêt pour la recherche. Une connaissance des langages Java, C+++ et Python est nécessaire. Des aptitudes à la rédaction technique et scientifique sont appréciées.

### Contexte

La thèse de Dorian Michaud, soutenue en novembre 2018, a permis d’étudier et de mettre en œuvre des algorithmes innovants dans le domaine de la recherche d’images par le contenu. Ces travaux de recherche ont abouti à la mise en place de deux applications : 
- une partie back office, catégorisant, indexant les images références et traitant les images reçues du front office.
- une partie front office fonctionnant sur des smartphones, ayant pour fonctionnalités la prise d’images et la soumission au back office. 

Le stage proposé, en collaboration avec un industriel leader mondial dans son domaine, s'appuie sur ces travaux existants.

### Problématique

Le projet proposé se situe dans le domaine de la recherche d’images basée sur le contenu visuel, qui est un domaine très actif de la vision par ordinateur du fait de l'augmentation constant du nombre de bases d’images disponibles. L’objectif de ce type d’approche est de retourner les images les plus proches d’une requête donnée en terme de contenu visuel. 

Il existe dans ce domaine de nombreuses techniques, mais les travaux de référence ont comme particularité de s’appliquer à des contextes où il n’y a pas ou peu de connaissances à priori sur les données (peu de vérité terrain). 

Par ailleurs, les solutions existantes nécessitent pour la plupart une connexion permettant de transmettre l'image requête à un serveur distant pour effectuer la recherche. Nous nous intéressons dans ce stage à la réalisation d'une preuve de concept d'un système de recherche d'images en contexte mobile sans connexion.

### Travail à effectuer

L’objectif est de mettre en oeuvre un système mobile de classification d'images, en s'appuyant sur les travaux existants. Dans un premier temps, il s’agira dans ce stage de se familiariser avec les travaux précédents, et de prendre en mains le cadre de travail.

Puis, il s'agira de constituer un jeu de données dit « expert », contenant des images de matériel électriques fournie par le partenaire industriel.

Ensuite, on proposera une adaptation des algorithmes du back office au contexte applicatif spécifique visé (classification d'images issues la base experte réalisée) afin d'optimiser le précision de classification.

Selon l'avancement des travaux, on procèdera enfin à un portage mobile du système permettant son utilisation sans connexion.

### Bibliographie

- [Michaud et al., 2018] Dorian Michaud, Thierry Urruty, Philippe Carré, François Lecellier: Adaptive features selection for expert datasets: A cultural heritage application. Sig. Proc.: Image Comm. 67: 161-170 (2018)

- [Michaud, 2018] Dorian Michaud : Indexation bio-inspirée pour la recherche d'images par similarité. Thèse de doctorat. http://www.theses.fr/s161772

- [Le et al., 2017] Huu Ton Le, Thierry Urruty, Syntyche Gbèhounou, François Lecellier, Jean Martinet, Christine Fernandez-Maloigne: Improving retrieval framework using information gain models. Signal, Image and Video Processing 11(2): 309-316 (2017)

- [Pérez et al., 2017] Luis Pérez, Jason Min Wang: The effectiveness of data augmentation in image classification using deep learning. ArXiv, 2017

- [Mennesson et al., 2014] José Mennesson, Pierre Tirilly, Jean Martinet: Elementary block extraction for mobile image search. ICIP 2014: 3958-3962

- [Urruty et al., 2014] Thierry Urruty, Syntyche Gbèhounou, Huu Ton Le, Jean Martinet, Christine Fernandez-Maloigne: Iterative Random Visual Word Selection. ICMR 2014: 249
