# Plateforme de simulation pour l'étude de la temporalité dans les interfaces

##  Présentation générale

Sujet pas encore affecté.

### Résumé

Le projet consiste à concevoir et implémenter une plateforme de tests permettant de simuler des interfaces graphiques courantes (desktop, mobile, etc.) en minimisant les latences d'affichage et de rafraichissement, et en permettant aux chercheurs de contrôler différents aspects visuels et temporels du comportement de l'interface. L'objectif est de produire une plateforme de test et d'expérimentation qui pourra être utilisée dans plusieurs projets de recherche sur la temporalité des interactions Homme-Machine (IHM):

-   simuler et évaluer les effets de différentes sources et amplitudes de latence sur les performances et l'utilisabilité d'une interface classique,
-   simuler des mises à jour visuelles survenant juste avant un clic, qui peuvent générer des erreurs de sélection ou d'activation,
-   permettre de faire passer des expériences contrôlées réalistes sur navigateur web.

### Encadrement

Équipe(s) : [Loki](http://loki.lille.inria.fr/)

Encadrant :

-   [Mathieu Nancel](http://mathieu.nancel.net)

[Contacter l'encadrant](mailto:mathieu@nancel.net).

Localisation : Inria Lille – Nord Europe, bâtiment B

## Présentation détaillée

### Pré-requis

Le candidat idéal est étudiant en master en informatique, et montre un grand intérêt pour la recherche en Interaction Homme-Machine.

Le candidat doit démontrer un intérêt pour l'IHM et des compétences en OpenGL ; des connaissances en WebGL sont un plus. Il devra faire preuve de créativité technique et conceptuelle.

### Description

La plateforme conçue devra être utilisable et réaliste, et permettre de scripter ses mises à jour visuelles : ajout de délais de réponse de l'interface, apparition ou disparition de fenêtres / widgets / notifications, simulation de ralentissement global de l'interface, etc. Elle devra aussi permettre de *logger* tous les événements d'entrée et d'affichage sans augmenter la latence, et de revenir à des états précédents de l'interface ou du système simulé.

La plateforme devra pouvoir se décliner sur différents dispositifs et environnements, avec un module central en OpenGL pouvant se décliner par exemple sous la forme d'une application de bureau, d'une application mobile, ou dans un navigateur web (WebGL) pour pouvoir faire passer des expériences contrôlées à un grand nombre de participants.

Les besoins en flexibilité et en finesse de contrôle des événements d'entrée et de l'affichage nécessiteront une réflexion poussée et de nouveaux modèles de gestion d'entrée/sortie en rupture avec les normes actuelles de conception d'interfaces.
L'étudiant aura l'opportunité d'appliquer les méthodes à la pointe de la littérature en IHM pour la mesure de latence en temps réel [1].

Si le projet avance suffisamment rapidement, l'étudiant sera impliqué dans les travaux de recherche qui nécessitent cette plateforme de test, par exemple la conception d'expériences contrôlées permettant d'évaluer les effets de différents problèmes de temporalité dans les interfaces, et/ou la conception de techniques d'interaction permettant de détecter ou de réduire ces effets. Ce stage pourra se poursuivre sous la forme d'une thèse en IHM.

### Bibliographie

[1] http://mjolnir.lille.inria.fr/turbotouch/lagmeter/
