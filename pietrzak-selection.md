#   Outil de sélection pour texte sur document manuscrit scanné

##  Présentation générale

Sujet pas encore affecté.

### Résumé

Le stage s'inscrit dans un projet visant à concevoir un outil d'aide à la transcription de documents manuscrits anciens. Cet outil proposera une combinaison de méthodes interactives et automatiques. En effet les méthodes automatiques ne sont pas suffisantes, car premièrement elles requièrent des bases de connaissances établies à la main. Deuxièmement l'utilisateur doit avoir le contrôle sur la levée des ambiguïtés. Et troisièmement nous souhaitons que l'utilisateur gagne en compétence, ce qui ne sera possible que s'il a un rôle actif. Le premier outil interactif que nous étudions est la sélection de texte sur un scan de document manuscrit. Notre approche est un pinceau de sélection à 4 degrés de liberté : position x-y, seuil de luminosité et taille de sélection. Nos premières études sont prometteuses, mais il reste à évaluer cet outil expérimentalement.

### Mots-clés

Interaction Homme-Machine

### Encadrement

Équipe(s) : Loki

Encadrant(s) :

-   Thomas Pietrzak
-   Stéphane Huot

[Contacter les encadrants](mailto:thomas.pietrzak@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : Inria Lille – Nord Europe, bâtiment B

##  Présentation détaillée

### Pré-requis

Le candidat idéal est étudiant en master en informatique, et montre un grand intérêt pour la recherche en Interaction Homme-Machine. Il doit avoir de l'expérience ou un intérêt fort pour le développement logiciel, si possible en webGL ou Three.js. La créativité, indépendance, travail en équipe et des facultés de communication sont des avantages.

### Description

Le stage s'inscrit dans un projet visant à concevoir un outil d'aide à la transcription de documents manuscrits anciens. Cet outil proposera une combinaison de méthodes interactives et automatiques. En effet les méthodes automatiques ne sont pas suffisantes, car premièrement elles requièrent des bases de connaissances établies à la main. Deuxièmement l'utilisateur doit avoir le contrôle sur la levée des ambiguïtés. Et troisièmement nous souhaitons que l'utilisateur gagne en compétence, ce qui ne sera possible que s'il a un rôle actif.

Le premier outil interactif que nous étudions est la sélection de texte sur un scan de document manuscrit. Les outils de sélection classiques tels que les formes libres et les différentes *baguettes magiques* ne sont pas adaptées. Notre approche est un pinceau de sélection à 4 degrés de liberté : position x-y, seuil de luminosité et taille de sélection. Nos premières études sont prometteuses [alawoe2018], mais il reste à évaluer cet outil expérimentalement.

Pour aller plus loin, nous souhaitons étendre les sélections avec l'aspect temporel. Nous souhaitons permettre à l'utilisateur d'ajuster sa sélection a posteriori selon les paramètres qui ont été utilisés lors de sa création.

Le travail du candidat consistera à :

* Étudier les outils de sélection de pixels, en particulier pour le texte manuscrit.
* Définir un espace de conception pour les techniques de sélection de pixels.
* Évaluer la technique de sélection actuelle.
* Améliorer la technique de sélection, pour permettre la modification de l'historique.

### Bibliographie

* Alawoe, E., Pietrzak, T., Huot, S. Outil de sélection de texte manuscrit sur des documents numérisés. Symposium international francophone sur l’Ecrit et le document (SIFED 2018).
