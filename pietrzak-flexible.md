#   Retour tactile sur un stylet flexible

##  Présentation générale

Sujet pas encore affecté.

### Résumé

La recherche en Interaction Homme-Machine (IHM) explore constamment des nouvelles modalités d’entrée et de sortie. Les *dispositifs flexibles* offrent une nouvelle modalité d’entrée continue. Les recherches précédentes ont exploré de nouveaux gestes, nouvelles technologies de capteurs, de nouveaux facteurs de forme ou de nouvelles modalités de sortie pour donner plus de contrôle sur ce nouveau degré de liberté. Ce sujet vise à poursuivre ces efforts, et à intégrer ces aspects afin de rapprocher cette technologie de la phase d’industrialisation.

Pour ce stage nous souhaitons ajouter du retour tactile à un stylet qui utilise la courbure comme degré de liberté supplémentaire. En effet le retour immédiat d'information est une propriété essentielle de la *manipulation directe*, qui favorise l'utilisabilité.

### Mots-clés

Interaction Homme-Machine

### Encadrement

Équipe(s) : Loki

Encadrant(s) :

-   Thomas Pietrzak
-   Stéphane Huot
-   Audrey Girouard (Carleton University, Ottawa, Canada)

[Contacter les encadrants](mailto:thomas.pietrzak@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : Inria Lille – Nord Europe, bâtiment B ou Carleton University

##  Présentation détaillée

### Pré-requis

Le candidat idéal est étudiant en master en informatique, et montre un grand intérêt pour la recherche en Interaction Homme-Machine. Il doit avoir de l'expérience ou un intérêt fort pour le développement logiciel et matériel. La créativité, indépendance, travail en équipe et des facultés de communication sont des avantages.

### Description

La recherche en Interaction Homme-Machine (IHM) explore constamment des nouvelles modalités d’entrée et de sortie. Les *dispositifs flexibles* offrent une nouvelle modalité d’entrée continue. Les recherches précédentes ont exploré de nouveaux gestes [Warren2013], nouvelles technologies de capteurs, de nouveaux facteurs de forme [Fellion2017] ou de nouvelles modalités de sortie pour donner plus de contrôle sur ce nouveau degré de liberté [Lahey2011]. Ce sujet vise à poursuivre ces efforts, et à intégrer ces aspects afin de rapprocher cette technologie de la phase d’industrialisation.

Pour ce stage nous souhaitons ajouter du retour tactile à un stylet qui utilise la courbure comme degré de liberté supplémentaire. En effet le retour immédiat d'information est une propriété essentielle de la *manipulation directe*, qui favorise l'utilisabilité [Shneiderman1983].

Le travail du candidat consistera à :

* Étudier l'état de l'art sur la manipulation directe, les dispositifs flexibles, et la boucle sensori-motrice.
* Achever la conception et implémenter un stylet flexible à retour tactile.


### Bibliographie

* Fellion, N., Pietrzak, T., Girouard, A. FlexStylus: Leveraging bend input for pen interaction. UIST ’17, 375–385.
* Lahey, B., Girouard, A., Burleson, W., Vertegaal, R. PaperPhone: Understanding the use of bend gestures in mobile
devices with flexible electronic paper displays. CHI ’11, 1303–1312.
* Shneiderman, B. Direct manipulation: A step beyond programming languages. Computer. 16, 8 (Aug. 1983),
57–69.
* Warren, K., Lo, J., Vadgama, V., Girouard, A. Bending the rules: Bend gesture classification for flexible displays. CHI ’13, 607–610.
