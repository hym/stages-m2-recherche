#   ⟨Intitulé du stage⟩

##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé

⟨...⟩

### Mots-clés

⟨...⟩

### Encadrement

Équipe(s) : ⟨...⟩

Encadrant(s) :

-   ⟨...⟩

[Contacter les encadrants](mailto:⟨...⟩?subject=Stage%20de%20recherche).

Localisation : ⟨...⟩


##  Présentation détaillée

### Pré-requis

⟨...⟩

### Contexte

⟨...⟩

### Problématique

⟨...⟩

### Travail à effectuer

⟨...⟩

### Bibliographie

⟨...⟩
