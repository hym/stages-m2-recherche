#   Méthodes de deep Learning pour la segmentation/reconnaissance d’objets en temps réel dans un contexte INDOOR

##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé

Le projet proposé se situe dans le domaine du traitement et de l'analyse de l'image et de la vidéo. Il porte sur la reconnaissance et la segmentation des objets dans en contexte INDOOR en utilisant des méthodes de Deep Learning. L'objectif de ce stage est d'implémenter à terme une de ces méthodes sur un système embarqué pour augmenter l'interaction avec des robots. Pour cela, il faudra tenir compte des contraintes de ce genre de système : énergie, temps de calcul, mémoire, etc.  

### Mots-clés

Deep Learning, Reconnaissance d'Objets, Temps réel, Systèmes Embarqués

### Encadrement

Équipe : FOX, CRIStAL

Encadrant :

- José Mennesson

[Contacter les encadrants](mailto:jose.mennesson@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : IMT Lille-Douai - site de Lille, Cité scientifique, Rue Guglielmo Marconi, 59650 Villeneuve-d’Ascq

##  Présentation détaillée

### Pré-requis

Ce stage s'adresse aux étudiants de Master mention Informatique, idéalement IVI ou MOCAD, avec une spécialisation en traitement d'images et de vidéo et/ou en apprentissage. Une expérience en Deep Learning est fortement recommandée ainsi qu'une bonne connaissance du langage Python et/ou C/C++.

### Contexte

La reconnaissance d'objets consiste à concevoir des algorithmes permettant d'identifier et de localiser automatiquement le contenu visuel des images : des personnes, des voitures, du mobilier, etc. Ces dernières années, cette discipline a connu d'importants progrès grâce à l'utilisation du Deep Learning (apprentissage profond), qui est basé sur des réseaux de neurones artificiels pourvus d’un nombre important de couches de neurones [1]. 

### Problématique

Dans le cadre de leurs travaux de recherche, les enseignant-chercheurs de l’IMT Lille-Douai ont mis au point des robots intelligents se déplaçant automatiquement dans un bâtiment. Pour le moment, les capteurs du robot ne permettent pas de reconnaître les objets qui l’entourent. Pour augmenter le degré d’interaction entre le robot et son environnement, des caméras couleur et de profondeur ont été ajoutées à ces robots pour avoir des informations plus précises sur l’environnement immédiat du robot. De là est venu le besoin de reconnaître les objets présents dans un contexte INDOOR (table, chaise, fenêtres, murs, portes, etc.) [2]. Plusieurs contraintes sont alors à prendre en compte : l'aspect (quasi-)temps-réel des traitements, la puissance de calcul et l'énergie limitées, etc. 
Des méthodes récentes de Deep Learning permettent de prendre en compte ces aspects [3].

### Travail à effectuer

Dans la cadre de ce stage, le stagiaire devra :
- réaliser une étude bibliographique concernant les méthodes de Deep-Learning pour la reconnaissance et la segmentation d'objets dans un contexte INDOOR;
- implémenter une méthode d'apprentissage profond pour la segmentation/reconnaissance d'objets;
- proposer une méthode pour réduire le temps de calcul inhérent à l'utilisation de méthodes de Deep Learning;
- implémenter la solution sur un système embarqué.

### Bibliographie

[1] LeCun, Y., Bengio, Y., & Hinton, G. « Deep learning. ». Nature, 521(7553), 436, 2015.
[2] Couprie, C., Farabet, C., Najman, L., & LeCun, Y. « Indoor semantic segmentation using depth information. » arXiv preprint arXiv:1301.3572, 2013.
[3] Ren, S., He, K., Girshick, R. & Sun, J., « Faster R-CNN: Towards Real-Time Object Detection with Region Proposal Networks, » in IEEE Transactions on Pattern Analysis & Machine Intelligence, vol. 39, no. 6, pp. 1137-1149, 2017. 
