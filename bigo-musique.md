#   Élaboration de modèles pour l’aide à la composition au sein du logiciel Guitar Pro

##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé

Guitar Pro est un logiciel utilisé par de nombreux guitaristes pour transcrire leurs idées sur tablatures, et plus généralement sur partitions, lors de la composition d'un morceau de musique.
L’objectif de ce stage consiste à expérimenter des méthodes pour développer un système stimulant la créativité musicale de l’utilisateur en assistant de manière intelligente son processus de composition.

### Mots-clés

informatique musicale, génération automatique de musique, apprentissage automatique, intelligence artificielle

### Encadrement

Équipe(s) : Équipe [Algomus](http://www.algomus.fr/) au CRIStAL et [Arobas Music](https://www.guitar-pro.com/fr/index.php?pg=apropos)

Encadrant(s) :

- Louis Bigo (Algomus)
- Mathieu Giraud (Algomus)
- Nicolas Martin (Arobas Music)

[Contacter les encadrants](mailto:louis.bigo@univ-lille.fr,mathieu.giraud@univ-lille.fr,nicolas.martin@arobas-music.com?subject=Stage%20de%20recherche).

Localisation : CRIStAL (bâtiment M3) et Arobas Music (Lille, Euratechnologies)


##  Présentation détaillée

### Pré-requis

- bonnes compétences en programmation
- aisance avec le langage Python
- intérêt pour l'intelligence artificielle
- intérêt et si possible pratique de la musique

### Contexte

Ce stage s’inscrit dans le cadre d’un co-encadrement entre l’équipe Algomus (Université de Lille, CRIStAL) et la société Arobas Music. L’équipe d’informatique musicale Algomus, collaboration entre les laboratoires CRIStAL (UMR CNRS 9189, Université de Lille) et MIS (UPJV, Amiens) s’intéresse à l’analyse informatique de partitions musicales. La société lilloise Arobas Music développe le logiciel Guitar Pro dédié à la saisie et à la consultation de tablatures de guitare, et plus généralement de partitions musicales. Plus de 10 millions de musiciens l’utilisent régulièrement pour l’apprentissage, la transcription ou la composition. La société s’intéresse à l’ajout de nouvelles fonctionnalités pour assister de manière intelligente le processus d’écriture et de composition de l’utilisateur. Arobas Music produit également la base de données mySongBook réunissant plus de 2000 tablatures de haute qualité.


### Problématique

Outre ses fonctionnalités dédiées à l'apprentissage de pièces de guitare, Guitar Pro est largement utilisé dans un contexte de composition. La composition musicale est une tâche au cours de laquelle le compositeur doit régulièrement résoudre des problèmes éventuellement causés par un manque de connaissance en théorie de la musique, un manque de familiarité avec le style musical visé ou tout simplement par un manque d'inspiration. Il peut par exemple s'agir de conclure une mélodie ou une séquence d'accords, modifier une région de la partition, ou encore modifier un rhythme pour se rapprocher d'un certain style.

### Travail à effectuer

Un premier objectif pourrait consister en un système proposant la continuation ou la terminaison d’une phrase musicale entamée. La construction d’un modèle reflétant un ensemble de tendances mélodiques, harmoniques et rythmiques associées à style musical peut s’effectuer par apprentissage automatique sur des phrases saisies par l’utilisateur ou sur des bases de données préexistantes, notamment le répertoire mySongBook maintenu par Arobas Music. Le stage commencera par dresser un état de l’art sur la génération automatique de tablatures, et plus généralement de musique. Il s’agira ensuite d’identifier une représentation pertinente des données afin de tester le potentiel de différentes méthodes issues de l’apprentissage automatique, en particulier les réseaux de neurones, sur le corpus mySongBook.


### Bibliographie

* N. dos Santos Cunha, A. Subramanian & D. Herremans. Generating guitar solos by integer programming. Journal of the Operational Research Society 69.6 (2018)
* M. McVicar, F. Satoru and G. Masataka. "AutoLeadGuitar : Automatic generation of guitar solo phrases in the tablature space." Signal Processing (ICSP), 2014 12th International Conference on. IEEE (2014)
* J. P. Briot, G. Hadjeres, and F. Pachet. Deep learning techniques for music generation — A survey. https://arxiv.org/abs/1709.01620 (2017)
* D. Herremans, C. Ching-Hua, and E. Chew. A functional taxonomy of music generation systems. ACM Computing Surveys 50/5, 69 (2017)
