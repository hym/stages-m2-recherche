#   Étude et conception de fonctions de transfert pour les tâches de rotations 3D

##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé

La trackball virtuelle est la technique la plus souvent utilisée dans beaucoup de logiciels et de sites web pour les rotations d'objets 3D [2, 4, 8]. Elle consiste à définir une sphère qui englobe l'objet à manipuler. Les déplacements du pointeur de la souris sont ensuite interprétés comme des rotations de la sphère et de l'objet 3D associé. Cette technique d'interaction a des paramètres cachés comme le rayon de la sphère pour lequel nous émettons l'hypothèse qu'il affecte les performances de la tâche. L'objectif est d'étudier de manière systématique cette technique d'interaction pour guider la conception de fonctions de transfert non linéaires, cela dans différents contextes : interaction avec une souris informatique, interaction tactile, et rotations physiques 3D. 

### Mots-clés

IHM, 3D

### Encadrement

Équipe(s) : Loki

Encadrant(s) :

-   [Géry Casiez](http://cristal.univ-lille.fr/~casiez/)

[Contacter les encadrants](mailto:gery.casiez@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : Inria Lille – Nord Europe, bâtiment B


##  Présentation détaillée

### Pré-requis

Idéalement, le candidat est un étudiant de master informatique, et montre un grand intérêt pour la recherche en Interaction Homme-Machine. Des aptitudes à la rédaction d'articles scientifiques seront appréciées.

### Contexte

Les fonctions de transfert sont utilisées pour toute interaction indirecte avec du contenu interactif. Ces fonctions définissent la relation entre les actions effectuées par un utilisateur dans un espace moteur et les actions résultantes dans un espace visuel. Par exemple une fonction de transfert pour le pointage décrit la relation entre les déplacements physiques de la souris et ceux du pointeur à l'écran [1]. Pour le pointage, les travaux de la littérature montrent que les fonctions non linéaires améliorent les performances des utilisateurs par rapport aux fonctions linéaires [1].

### Problématique

Les fonctions de transfert pour les rotations définissent la relation entre les actions de l'utilisateur sur un périphérique physique et les rotations résultantes de l'objet virtuel manipulé. Si des fonctions à base de gains constants ont déjà été définies et utilisées dans les travaux de la littérature, l'influence de telles fonctions sur les performances des utilisateurs reste à mesurer [5]. La conception de fonctions non-linéaires reste à explorer et analyser.

### Travail à effectuer

Après une étude de la littérature sur les techniques de rotations 3D et fonctions de transfert, le candidat concevra et mènera une expérience contrôlée dans le but d'étudier de manière systématique les paramètres associés aux techniques de trackball virtuelles. Les résultats permettront de guider la conception de fonctions non-linéaires. Différents contextes seront considérés : interaction avec une souris [3], interaction tactile [6, 7] et rotations 3D à l'aide de capteurs 6 degrés de liberté.

### Bibliographie

[1]	Casiez, G. and Roussel, N. 2011. No More Bricolage!: Methods and Tools to Characterize, Replicate and Compare Pointing Transfer Functions. Proceedings of the 24th Annual ACM Symposium on User Interface Software and Technology (New York, NY, USA, 2011), 603–614.

[2]	Chen, M., Mountford, S.J. and Sellen, A. 1988. A Study in Interactive 3-D Rotation Using 2-D Control Devices. Proceedings of the 15th Annual Conference on Computer Graphics and Interactive Techniques (New York, NY, USA, 1988), 121–129.

[3]	DeLong, S. and MacKenzie, I.S. 2018. Evaluating Devices for Object Rotation in 3D. Universal Access in Human-Computer Interaction. Methods, Technologies, and Users (2018), 160–177.
 
[4]	Henriksen, K., Sporring, J. and Hornbaek, K. 2004. Virtual trackballs revisited. IEEE Transactions on Visualization and Computer Graphics. 10, 2 (Mar. 2004), 206–216. DOI:https://doi.org/10.1109/TVCG.2004.1260772.

[5]	Poupyrev, I., Weghorst, S. and Fels, S. 2000. Non-isomorphic 3D Rotational Techniques. Proceedings of the SIGCHI Conference on Human Factors in Computing Systems (New York, NY, USA, 2000), 540–547.

[6]	Rousset, É., Bérard, F. and Ortega, M. 2014. Two-finger 3D Rotations for Novice Users: Surjective and Integral Interactions. Proceedings of the 2014 International Working Conference on Advanced Visual Interfaces (New York, NY, USA, 2014), 217–224.

[7]	Scheurich, D. and Stuerzlinger, W. 2013. A One-handed Multi-touch Mating Method for 3D Rotations. CHI ’13 Extended Abstracts on Human Factors in Computing Systems (New York, NY, USA, 2013), 1623–1628.

[8]	Shoemake, K. 1992. ARCBALL: A User Interface for Specifying Three-dimensional Orientation Using a Mouse. Proceedings of the Conference on Graphics Interface ’92 (San Francisco, CA, USA, 1992), 151–156.
