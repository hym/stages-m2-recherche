# Augmentation de données intelligente pour l'analyse du visage

##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé
Les méthodes d'analyse faciale (reconnaissance d'expressions, détection de points d'intérêt...) reposent majoritairement sur des méthodes d'apprentissage profond, qui requièrent de grandes quantités de données annotées pour fonctionner. L'objectif de ce stage est d'étudier des méthodes permettant de générer automatiquement des données additionelles pertinentes afin d'améliorer les performances des systèmes existants à moindre coût.

### Mots-clés
Analyse faciale, apprentissage profond, deep learning, augmentation de données, génération d'images et de vidéos

### Encadrement

Équipe : Fox

Encadrant :

- Pierre Tirilly

[Contacter les encadrants](mailto:pierre.tirilly@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : IMT Lille Douai, Cité scientifique, 20 rue Guglielmo Marconi, 59650 Villeneuve d'Ascq


##  Présentation détaillée

### Pré-requis
Ce stage s'adresse à un étudiant de niveau Bac+5 (Master 2, école d'ingénieurs) en Informatique ou dans une discipline connexe (mathématiques appliquées, statistiques...), avec des compétences dans au moins un des domaines suivants :
 - analyse d'images et de vidéos ;
 - apprentissage artificiel.

Autres compétences souhaitées :
 - bon niveau en programmation (essentiellement Python) ;
 - maîtrise de l'anglais écrit ;
 - compétences en rédaction scientifique.

### Contexte
Les méthodes d'analyse faciale (reconnaissance du visage, d'expressions...) revêtent une importance particulière dans un nombre croissant de domaines : marketing, médecine, interaction homme-machine... Les systèmes d'analyse faciale reposent essentiellement sur des méthodes d'apprentissage artificiel (machine learning) supervisées : par exemple, un système de reconnaissance d'expressions apprendra à associer l'expression particulière (joie, colère, tristesse...) représentée sur une image de visage en optimisant une modèle statistique à partir d'images de visages dont l'expression est connue. En particulier, comme dans beaucoup d'autres champs de la vision par ordinateur, les méthodes récentes d'analyse faciale reposent essentiellement sur des modèles d'apprentissage profond (deep learning) [1,2]. Ces modèles offrent d'excellentes performances mais nécessitent d'importantes quantités de données annotées. Obtenir de telles quantités de données est extrêmement coûteux car elles doivent être préalablement annotées par des humains. Une alternative est d'avoir recours à de l'augmentation de données, c'est-à-dire à générer automatiquement de nouvelles images d'apprentissage à partir des images annotées disponibles.

### Problématique
Les méthodes courantes d'augmentation de données se contentent d'appliquer une gamme de transformations élémentaires (symétrie, rotation...) aux images. Ces transformations sont définies a priori. Cette approche a deux limites :
 1. les transformations optimales à utiliser en fonction de la tâche et du contexte applicatif considérés ne sont pas connues ;
 2. les nouvelles images générées ne présentent qu'une variété limitée et restent très proches des données originales.

Pour pallier ce problème, deux pistes peuvent être envisagées :
 1. sélectionner automatiquement les transformations les plus appropriées en fonction de l'objectif recherché [3].
 2. générer des images grâce à des transformations plus complexes que de simples opérations géométriques [4,5,6,7] : transfert d'identité, modification d'attributs faciaux...

### Travail à effectuer
Ce stage s'intéresse à la seconde piste : l'objectif est de développer une méthode d'augmentation de données pour l'analyse faciale reposant sur la génération automatique de transformations complexes du visage. Le travail se concentrera autour de l'une des deux applications suivantes : la détection de points fiduciaires (nez, bouche...) ou la reconnaissance d'expressions.

Les étapes à réaliser seront les suivantes :
 - état de l'art des méthodes de génération d'images de visages et des problématiques connexes (génération de vidéos d'actions et d'images de personnes) ;
 - sélection de méthodes appropriées de l'état de l'art, et le cas échéant, adaptation de ces méthodes aux objectifs visés ;
 - implémentation de prototypes ;
 - évaluation des méthodes développées.

Chaque étape s'accompagnera de la rédaction de la documentation appropriée.

### Bibliographie
[1] R. Belmonte et al., Video-based Face Alignment with Local Motion Modeling, WACV, 2019.
[2] K. Zhang et al., Facial Expression Recognition Based on Deep Evolutional Spatial-temporal Networks. IEEE Transactions on Image Processing, 26(9):4193–4203, 2017.
[3] X. Peng et al., Jointly Optimize Data Augmentation and Network Training: Adversarial Data Augmentation in Human Pose Estimation. CVPR, 2018.
[4] J. Kossaifi et al., GAGAN: Geometry-Aware Generative Adversarial Networks. CVPR, 2018.
[5] O. Wiles et al., X2Face: A Network for Controlling Face Generation Using Images, Audio, and Pose Codes. ECCV, 2018.
[6] C. Yang et al., Pose Guided Human Video Generation. ECCV, 2018.
[7] G. Zhang et al., Generative Adversarial Network with Spatial Attention for Face Attribute Editing, ECCV, 2018.